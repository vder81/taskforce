package taskforce.model

trait ResourceId[A] {
  val value: A
}
